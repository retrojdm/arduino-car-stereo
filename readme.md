Arduino Car Stereo
==================

By Andrew Wyatt

retrojdm.com


Visit the following link for build photos and a demo video:

http://www.retrojdm.com/ArduinoCarStereo.asp


Why?
----

This whole thing started because I wanted MP3's in my RA28 Celica, but didn't want to ruin the interior by adding a modern 1-DIN sterero.

After going to great lengths to replace the previously molested dash, there's no way I was going to cut into my original center console.

Now, there's already a few oldschool looking stereos on the market (like the RetroSound), but they don't make any that fit the old Toyota hole size.

### For anyone interested...

--------------------------------------------------------------------------------

The hole in the dash is 85.5 x 32mm (3.37 x 1.25 in.)

The tuning and volume knob holes are 11mm (0.43 in.) diameter, and 123mm (4.85 in.) apart.

Obviously I could have hidden something in the glovebox, but I wanted to be able to reach the controls. There's plenty of other ways I could have achieved my goal, but as a bit of a geek, I couldn't help myself. I had to try building one using an Arduino.

--------------------------------------------------------------------------------


Features
--------

The beauty of doing this with an Arduino is I can always change it later. It's like Lego. But as tempting as it is to keep adding features, at some point I have to actually make the thing.

So for now, I've settled on the following:
* FM Tuner
* Line-in
* USB (MP3/WMA)
* Clock
* 12-character alphanumeric display
* Bass / Treble / Balance
* Display brightness
* Spectrum Analyzer with 5x display modes (lines, bars, peaks, pulse, bars+info)


The unit itself has no amplifier. It outputs at line level. I've got it connected to an Alpine KTP-445U 4-channel amp.

The choice in display was a hard one. Those little Avago HCMS-29xx displays are expensive! But for the 80s look, I absolutely had to do it with LEDs.

I don't regret a thing.


A Tribute to the 1980s Toyota AM/FM/MPX Electronic Tuner
--------------------------------------------------------

You may have noticed the similarity between my face design, and the radio in Toyota's MA47 Supra (part number 86120-14350). This is not a coincidence.


So, What's It Made Of?
----------------------

Well, my project uses:
* Arduino Mega 2560
* Custom Printed Circuit Boards by OSH park
* Si4703 FM Tuner breakout board
* VMusic3 USB/MP3 module
* PT2314 Audio Processor IC (4x inputs, bass, treble, balance, and volume control)
* DS1307 Realtime clock
* 3x HCMS-2904 LED displays for a total of 12 alphanumeric characters (the photos don't do them justice - they're much brighter in real life)
* MSGEQ7 Spectrum Analyzer
* A whole bunch of wires, buttons, lights, and knobs :)
* A broken shaft-style radio from a '79 Supra as the case
* Aluminium front panel milled, engraved, and in-filled by frontpanelexpress.com
* Switch-mode DC/DC Converter (TRSN 1-2490) to supply 9V through the barrel-jack instead of the 12-14V a car generates. This stops the Arduino's on-board 5V voltage regulator from overheating and resetting.


Still Yet To Do
---------------

* Have the display screen remade in a tinted acrylic (rather than the clear it is now).
* Maybe repaint the buttons in a harder wearing paint, or even 3D print them in stainless steel!