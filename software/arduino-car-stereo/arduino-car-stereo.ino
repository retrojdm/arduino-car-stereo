/*-------------------+
| Arduino Car Stereo |
+--------------------+


BUGS:
  * Removing the USB without pressing stop causes problems.
      This had been solved before the refactor.
      See waitForPrompt() in old commits in bitbucket
          
  * Track totals are double what they should be.


////////////////////////////////////////////////////////////////////////////////
A0 is dead on my Mega. So I'm using A14 instead.
////////////////////////////////////////////////////////////////////////////////

Andrew Wyatt
retrojdm.com

Started: 2012-07-18
Updated: 2017-06-23

Last Changes: #define SERIAL_MONITOR

*/

#include "EEPROM.h"             // Load and Save settings

#include "Wire.h"               // I2C connection for the:
                                //  - Si4703 FM Radio         http://www.sparkfun.com/products/10775
                                //  - PT2314 Audio Processor  http://www.datasheetcatalog.org/datasheet/PrincetonTechnologyCorporation/mXuyuxw.pdf
                                //  - DS1307 Realtime clock   https://www.sparkfun.com/products/99

#include <hcms29xx.h>           // 3x HCMS-29xx (4-char 5x7 LED displays). Total of 12 characters.

// Si4703 FM Radio
// Connect via a Logic Level Converter.
// If you supply this board with 5V you'll kill it (it's a 3.3V board).
#include <si4703.h>

#include <pt2314.h>             // Audio Processor IC with: 4 inputs, Volume, Bass, Treble, and Balance

#include <vmusic3.h>            /* VMusic3 USB/MP3 module.
                                   Switched using an NPN Transistor (Eg: BC548).
                                   Collector = Arduino 5V
                                   Base      = Arduino digital IO pin (see PIN_VMUSIC_5V below)
                                   Emitter   = VMusic 5V
                                */

// Uncomment this line to enable output to the serial monitor (at 9600 baud).
#define SERIAL_MONITOR

// EEPROM Addresses
#define SETTINGS_ADDRESS          64
#define SETTINGS_ID_STRING        "AET 1.0"
// Pinouts
#define BUTTONS                   14 // How many buttons are there?
#define BUTTON_DEBOUNCE_DURATION  20 // milliseconds
#define BUTTON_HOLD_DURATION     500 // milliseconds

#define BTN_SOURCE        A14  // Rotary encoder button. Should be A0 to match the main PCB, but A0 is dead on my Mega. I've bypassed it with a jumper wire.

#define BTN_DISPLAY       A1  // Slim button
#define BTN_MUTE          A2  // Slim button

#define BTN_PRESET0       A3  // 4mm Square button
#define BTN_PRESET1       A4  // 4mm Square button
#define BTN_PRESET2       A5  // 4mm Square button
#define BTN_PRESET3       A6  // 4mm Square button
#define BTN_PRESET4       A7  // 4mm Square button
#define BTN_PRESET5       A10 // 4mm Square button

#define BTN_PLAY          A3  // 4mm Square button
#define BTN_PAUSE         A4  // 4mm Square button
#define BTN_STOP          A5  // 4mm Square button
#define BTN_RANDOM        A6  // 4mm Square button
#define BTN_REPEAT        A7  // 4mm Square button

// Error here? You've probably got the board set as an UNO instead of a MEGA :)
#define BTN_SEEKDOWN      A8  // 9mm Square button
#define BTN_SEEKUP        A9  // 9mm Square button

#define BTN_FOLDER        A10  // 4mm Square button

#define BTN_MENU          A11  // Slim button
#define BTN_BACK          A12  // Slim button

#define BTN_POWER         A13  // Rotary Encoder button

#define PRESETS            6   // How many presets are there?
#define NO_PRESET         -1

#define LED_PRESET0       48
#define LED_PRESET1       49
#define LED_PRESET2       50
#define LED_PRESET3       51
#define LED_PRESET4       52
#define LED_PRESET5       53

#define LED_PLAY          48
#define LED_PAUSE         49
#define LED_STOP          50
#define LED_RANDOM        51
#define LED_REPEAT        52
#define LED_FOLDER        53

#define LED_DEBUG         13



// Rotary Encoder Pins (and their interrupts)
// INT    0   1   2   3   4   5
// PIN    2   3  21  20  19  18

#define ENC_TUNING_A       3
#define ENC_TUNING_B       2
#define INT_TUNING_A       1 // Interrupt 1 = Pin 3
#define INT_TUNING_B       0 // Interrupt 0 = Pin 2

#define ENC_VOLUME_A      19
#define ENC_VOLUME_B      18
#define INT_VOLUME_A       5 // Interrupt 5 = Pin 18
#define INT_VOLUME_B       4 // Interrupt 4 = Pin 19

#define POSITIONS_PER_DETENT 4
#define DETENT_SETTLE_DELAY  500



#define MIN_FREQUENCY       885
#define MAX_FREQUENCY      1080
#define FREQUENCY_STEP        1
#define SEEK_DISPLAY_DELAY   40 // milliseconds to wait before displaying the frequency again while seeking
#define RDS_TIMEOUT         200 // Give up trying to read an RDS message after this many milliseconds
#define RDS_REFRESH_DELAY  1000 // How long before checking again?

// Sources and Modes
#define SOURCES               3
#define SOURCE_TUNER          0
#define SOURCE_USB            1
#define SOURCE_LINEIN         2

#define DISPLAY_MODES         4
#define DISPLAY_MODE_SHORT    0
#define DISPLAY_MODE_LONG     1
#define DISPLAY_MODE_CLOCK    2
#define DISPLAY_MODE_SPECTRUM 3

#define SPECTRUM_DELAY       20 // Milliseconds to wait before refreshing the spectrum analyzer.
                                //  10 ms = 100 fps
                                //  20 ms =  50 fps
                                //  40 ms =  25 fps
                                // 100 ms =  10 fps

#define INPUT_MODE_NORMAL     0 // Depends on settings.source
#define INPUT_MODE_MENU       1
#define INPUT_MODE_BASS       2
#define INPUT_MODE_TREBLE     3
#define INPUT_MODE_BALANCE    4
#define INPUT_MODE_GRAPH_STYLE 5
#define INPUT_MODE_BRIGHTNESS 6
#define INPUT_MODE_HOURS      7
#define INPUT_MODE_MINUTES    8

#define MUTE                   0x3F
#define MIN_VOLUME_ENC            0 // Displayed / encoder stops
#define MAX_VOLUME_ENC           15 // Displayed / encoder stops
#define MIN_VOLUME_PT2314        24 // Sent to the PT2314 (0..63)
#define MAX_VOLUME_PT2314        63 // Sent to the PT2314 (0..63)


// 3x HCMS-2904's Cascaded
#define DISPLAY_LENGTH           12 // Characters
#define CHARACTER_WIDTH           5
#define CHARACTER_HEIGHT          7
#define DISPLAY_BUFFER          255 // Characters
#define PIN_DISPLAY_DATA         22
#define PIN_DISPLAY_RS           23
#define PIN_DISPLAY_CLK          24
#define PIN_DISPLAY_CE           25
#define PIN_DISPLAY_RESET        26


// 5V Relay for sending a 12V "Remote Power On" signal to am external Amplifier.
#define PIN_AMP_POWER            45



// MSGEQ7 Spectrum Analyzer IC
// I've connected LIN and RIN on the PT2314 to the IN pin on the MSGEQ7,
// following the "Typical Application" schematic on page 4 of the MSGEQ7 datasheet.
#define PIN_MSGEQ7_RESET         46
#define PIN_MSGEQ7_STROBE        47
#define PIN_MSGEQ7_OUT          A15
#define PEAK_HOLD_FRAMES         25 // How many frames
#define MSGEQ7_NOISE_CUTOFF      96 // 0..1023 - discard readings below this value - to eliminate noise in the graph
#define MSGEQ7_MINIMUM_SCALE    384 // 0..1023 - the minimum bar height scale
#define MSGEQ7_SCALE_SAMPLES    100 // Store the highest reading for the last x frames. See SPECTRUM_DELAY for the frame rate.

#define GRAPH_STYLES              5
#define GRAPH_STYLE_LINES         0
#define GRAPH_STYLE_BARS          1
#define GRAPH_STYLE_PEAKS         2
#define GRAPH_STYLE_PULSE         3
#define GRAPH_STYLE_TRACK_TIME    4

// Temporary messages
#define MESSAGE_DURATION       1250 // milliseconds to display messages for (or wait after scrolling)
#define MESSAGE_SCROLL_STEP      75 // milliseconds to wait before scrolling another character
#define MESSAGE_SCROLL_PAUSE    750 // How long to wait before scrolling

#define MESSAGE_MUTE          "    Mute    "
#define MESSAGE_LINEIN        "  Line-in   "
#define MESSAGE_LINEIN_SPECTRUM "Line"

#define MESSAGE_USB_NO_DISK   "    USB     "
#define MESSAGE_USB_NO_DISK_SPECTRUM   "         USB"
#define MESSAGE_SEARCHING     "Searching..."
#define MESSAGE_USB_NO_TRACKS "No Tracks :("

#define MESSAGE_USB_PAUSE     "   Pause    "
#define MESSAGE_USB_STOP      "    Stop    "
#define MESSAGE_USB_RANDOM    " Random" // :On|Off
#define MESSAGE_USB_REPEAT    " Repeat" // :On|Off


#define MESSAGE_USB_DEVICE_NAME "USB:"

// Clock
#define DS1307_I2C_ADDRESS    0x68  // This is the I2C address for the Realtime clock
#define CURSOR_FLASH_DURATION  500  // milliseconds
#define CURSOR_POSITIONS         2
#define NO_CURSOR                0
#define CURSOR_HOURS             1
#define CURSOR_MINUTES           2

// Radio
#define PIN_TUNER_RESET          4
#define PIN_SDA                 20
#define PIN_SDL                 21



// VMusic3 USB/MP3 Module
#define PIN_VMUSIC_5V            5  // to base of an NPN Transistor (Eg: BC548) that switches the VMusic's 5V power (red)
                                    // (a transistor is used because digital IO pins don't provide enough current to run the VMusic3)                                    
// SPI pins
#define PIN_VMUSIC_MISO          6  // to VMusic MISO (brown)
#define PIN_VMUSIC_MOSI          7  // to VMusic MOSI (orange)
#define PIN_VMUSIC_SCK           8  // to VMusic CLK  (yellow)
#define PIN_VMUSIC_SS            9  // to VMusic SS#  (green)

/*-----------+
| Structures |
+-----------*/

typedef struct ButtonType {
    byte          pin;
    boolean       was;
    boolean       is;
    unsigned long lastPress;
    boolean       held;
};

typedef struct EncoderType {
    byte pinA;             // set either low or high
    byte pinB;
    byte was;              // the previous state of the encoder.
    byte is;               // = (digitalRead(pinB) * 2) + digitalRead(pinA)
    signed char positions; // number of positions turned since last call to readEncoder(). Set via updateEncoder() using an interrupt.
    unsigned long lastUpdate;                            
};

typedef struct DateTimeType {
    byte second;
    byte minute;
    byte hour;
    byte dayOfWeek;
    byte dayOfMonth;
    byte month;
    byte year;
};

typedef struct SettingsType {
    char        id[8];
    boolean     standby;
    byte        source;
    byte        displayMode;
    byte        graphStyle;
    signed char currentPreset;
    int         presetFrequency[PRESETS];
    int         currentFrequency;
    int         currentVolume;
    int         bass;
    int         treble;
    int         balance;
    int         brightness;
    boolean     random;
    boolean     repeatFolder;
};

typedef struct LastTrackType {
    char        path    [VMUSIC_PATH_LENGTH + 1];
    char        filename[13];
    int         trackNumber;
};

typedef struct MenuItemType {
    char    *name;
    int     inputMode;
};


/*-----------------+
| Global Variables |
+-----------------*/

// 3x HCMS-2904 displays. Total of 12 characters.
hcms29xx myDisplay(PIN_DISPLAY_DATA, PIN_DISPLAY_RS, PIN_DISPLAY_CLK, PIN_DISPLAY_CE, PIN_DISPLAY_RESET, DISPLAY_LENGTH);
int intMaxLeft = 0; // How far left can we scroll left? (negative for messages wider than the screen)


// Splash Screens generated by LCD Assistant
// Make sure you use a 60 x 8 pixel 2-colour bitmap.
// http://en.radzio.dxp.pl/bitmap_converter/

/*
// Arduino.cc
byte splashScreen[] = {
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x70, 0x7E, 0x27, 0x7E, 0x70,
    0x7F, 0x7F, 0x0B, 0x7F, 0x76,
    0x7F, 0x7F, 0x63, 0x7F, 0x3E,
    0x3F, 0x7F, 0x60, 0x7F, 0x3F,
    0x63, 0x7F, 0x7F, 0x63, 0x00,
    0x7F, 0x7E, 0x1C, 0x3F, 0x7F,
    0x3E, 0x7F, 0x63, 0x7F, 0x3E,
    0x40, 0x00, 0x70, 0x50, 0x50,
    0x70, 0x50, 0x50, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00
};
*/

// RetroJDM.com
byte splashScreen[] = {
    0x7F, 0x7F, 0x0B, 0x7F, 0x76,
    0x7F, 0x7F, 0x6B, 0x6B, 0x63,
    0x03, 0x7F, 0x7F, 0x03, 0x00,
    0x7F, 0x7F, 0x0B, 0x7F, 0x76,
    0x3E, 0x7F, 0x63, 0x7F, 0x3E,
    0x00, 0x60, 0x60, 0x7F, 0x3F,
    0x7F, 0x7F, 0x63, 0x7F, 0x3E,
    0x7F, 0x0E, 0x1C, 0x0E, 0x7F,
    0x00, 0x00, 0x40, 0x00, 0x00,
    0x00, 0x70, 0x50, 0x50, 0x00,
    0x00, 0x70, 0x50, 0x70, 0x00,
    0x70, 0x10, 0x70, 0x10, 0x70
};

// Spectrum Analyzer Peaks.
int peak[DISPLAY_LENGTH];
int peakTimer[DISPLAY_LENGTH];

// Spectrum Analyzer samples.
// This is scale the height of the graph to accomodate different recording levels.
// Each frame, we store the highest reading of the 7 frequencies from the MSGEQ7.
// This goes into an array, where a position index is constantly cycling.
// Each frame we take the average of all the readings, and set that as the scale for the spectrum analyzer.
int scaleSamples[MSGEQ7_SCALE_SAMPLES];
byte scaleIndex = 0;


// Create an instance of the Si4703 FM Receiver
Si4703 tuner(PIN_TUNER_RESET, PIN_SDA, PIN_SDL);

// VMusic3 SPI library
VMusic3 vmusic(PIN_VMUSIC_MISO, PIN_VMUSIC_MOSI, PIN_VMUSIC_SCK, PIN_VMUSIC_SS);

// For 4 audio inputs, Volume, Bass, Treble, and Balance
PT2314 audioProcessor;

// Clock
DateTimeType  objDateTime;
unsigned long lngMillisCursorFlash = 0;
boolean       bCursor = false;


// Put the Buttons and preset LEDs in arrays so we can cycle through them with a for loop.
ButtonType button[] = {
    {BTN_SOURCE,   false, false, 0, false},
    {BTN_MENU,     false, false, 0, false},
    {BTN_BACK,     false, false, 0, false},
    {BTN_PRESET0,  false, false, 0, false},
    {BTN_PRESET1,  false, false, 0, false},
    {BTN_PRESET2,  false, false, 0, false},
    {BTN_PRESET3,  false, false, 0, false},
    {BTN_PRESET4,  false, false, 0, false},
    {BTN_SEEKDOWN, false, false, 0, false},
    {BTN_SEEKUP,   false, false, 0, false},
    {BTN_PRESET5,  false, false, 0, false},
    {BTN_DISPLAY,  false, false, 0, false},
    {BTN_MUTE,     false, false, 0, false},
    {BTN_POWER,    false, false, 0, false}
};
byte presetLEDs[] = {LED_PRESET0, LED_PRESET1, LED_PRESET2, LED_PRESET3, LED_PRESET4, LED_PRESET5};

// Global tuning and volume variables
EncoderType encTuning, encVolume;

// Tuner state variables
boolean bMute               = false;
boolean bSeeking            = false;
long    lngSeekDisplayTimer = 0;
boolean bValid              = false;

// USB/MP3 Player state variables
boolean bFolder             = false;
boolean bResume             = false; // Do we need to unpause when cycling through sources?

// Menu state variables
byte bytInputMode = INPUT_MODE_NORMAL;
int  intMenuItem  = 0;
#define MENU_ITEMS 6
MenuItemType menuItems[] = {
  { "Bass", INPUT_MODE_BASS },
  { "Treble", INPUT_MODE_TREBLE },
  { "Balance", INPUT_MODE_BALANCE },
  { "Clock", INPUT_MODE_HOURS },
  { "Graph Style", INPUT_MODE_GRAPH_STYLE },
  { "Brightness", INPUT_MODE_BRIGHTNESS }
};//"            " Max 12 chars.

char *graphStyles[GRAPH_STYLES] = { "Lines", "Bars", "Peaks", "Pulse", "Bar+T" }; // max 5 chars.

// Temporary message variables.
unsigned long lngMillisMessageFinished = 0;
unsigned long lngMillisScrollAgain     = 0;
boolean       bDisplayingMessage       = false;
char          strCurrentMessage[DISPLAY_BUFFER + 1];
char          strRDSMessage[DISPLAY_BUFFER + 1];

// This is purely for debugging. the LED_DEBUG is turned on when a button is pushed, and turned off when we get back to the main loop.
boolean       bButtonPushed            = false;

// The settings we load and save.
SettingsType settings;

// Loading and saving settings is too slow when we include these.
// We'll load and save them only when we need to.
LastTrackType lastTrack = { "", "", 0 };



/*--------------------+
| Function Prototypes |
+--------------------*/
// The Arduino IDE is a little tempromental when it comes to organising files in tabs.
// We need to provide prototypes in the main .INO file to avoid "... was not declared in this scope" errors.

// Buttons
void setupButtons               ();
void loopButtons                ();
void buttonPushed               (byte b);
void buttonHeld                 (byte b);
void powerButtonPushed          ();
void universalButtons           (byte pin);
void tunerButtons               (byte pin, boolean bHeld);
void usbButtons                 (byte pin);
void presetPushed               (byte preset, boolean bHeld);

// Clock
void setupClock                 ();
byte decToBcd                   (byte val);
byte bcdToDec                   (byte val);
void getDateTime                ();
void setDateTime                ();

// Display
void setupDisplay               ();
void loopDisplay                ();
void setBrightness              (int intBrightness);
void setDisplay                 (char *strDisplay);
void updateDisplayAndLEDs       ();
void waitForMessage             (long lngTimer);
void message                    (char *strMessage, long lngTimer);
void messageCurrentFrequency    ();
void messageTrackInfo           ();
void setDisplayBar              (char *strLabel, int intValue);
void setDisplayBalanceBar       (char *strLabel, int intValue);
void setDisplayCurrentFrequency ();
void setDisplayRDS              (boolean bForceRefresh);
void setDisplayTrackTime        ();
void displayTime                ();
void displaySettingTime         ();
void displaySetting             ();
void setupSpectrum              ();
void clearPeaks                 ();
void readAndDisplaySpectrum     ();
void resample                   (float *arrIn, int intInCount, float *arrOut, int intOutCount);

// Rotary Encoders
void setupRotaryEncoders        ();
void loopRotaryEncoders         ();
void initEncoder                (struct EncoderType *encEncoder, byte pinA, byte pinB);
int  readEncoder                (struct EncoderType *encEncoder);
void updateEncoder              (struct EncoderType *encEncoder);
void interruptTuning            ();
void interruptVolume            ();

// Settings
void saveSettings               ();
void loadSettings               ();
void saveLastTrack              ();
void loadLastTrack              ();

// Sources
void setupSource                ();
void loopSource                 ();
void initSource                 ();
void tryToLoadLastTrack         ();
void initAudioProcessor         ();
void selectTuner                ();
void deselectTuner              ();
void selectVMusic               ();
void deselectVMusic             ();
void selectLineIn               ();
void deselectLineIn             ();



/*------+
| Setup |
+------*/

void setup() {

    #ifdef SERIAL_MONITOR
        delay(500);
        Serial.begin(115200);
        Serial.println("Serial monitor ready.");
    #endif
      
    loadSettings(); // We need to load the settings before setting up the display for the brightness setting.
    loadLastTrack();
    
    setupDisplay();
    setupButtons();
    setupClock();
    setupRotaryEncoders();
    setupSource();
}


 
/*-----+
| Loop |
+-----*/

void loop() {
  
    if (bButtonPushed) {
        bButtonPushed = false;
        digitalWrite(LED_DEBUG, LOW);
    }
  
    loopButtons();
    loopRotaryEncoders();
    loopDisplay();
    loopSource();
}
