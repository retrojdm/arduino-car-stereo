/*--------+
| Buttons |
+---------+

void setupButtons        ()
void loopButtons         ()

void buttonPushed        ()
void buttonHeld          ()
void powerButtonPushed   ()
void universalButtons    (byte pin)
void tunerButtons        (byte pin, boolean bHeld)
void usbButtons          (byte pin)
void presetPushed        (byte preset, boolean bHeld)
*/



void setupButtons() {
    // The buttons connect a digital I/O pin to ground when pressed.
    // We need to use the Arduino's internal pull-up resistors to tell the difference between HIGH and LOW.
    for (byte b = 0; b < BUTTONS; b++) pinMode(button[b].pin, INPUT_PULLUP);
}



void loopButtons() {
        
    // cycle through each button...
    for (byte b = 0; b < BUTTONS; b++) {
   
   	// Buttons
	button[b].was	= button[b].is;
	button[b].is	= digitalRead(button[b].pin) == LOW;
   
        // Is the button down?
        if (button[b].is) {
    
            // Has the button NOT yet been flagged as "Held"?
            if (!button[b].held) {
                  
                // Has the button just changed to "on"?
                // (We know the button is down. If it wasn't down just before, then it must have been pressed)
                if (!button[b].was) {
                    button[b].held      = false;
                    button[b].lastPress = millis();
                }
    
                // Has the button been held long enough?
                if (millis() > button[b].lastPress + BUTTON_HOLD_DURATION) {
    
                    // Flag it as held. Trigger the OK event.
                    button[b].held = true;
                    buttonHeld(b);
                    
                    updateDisplayAndLEDs();
                }
            }
        }
    
        // Button just changed to "off".
        if (!button[b].is && button[b].was) {
    
            if (button[b].held) {
                button[b].held = false;
            }
            else {

                // Release Event
                buttonPushed(b);
                
                updateDisplayAndLEDs();

            }
        }
    }
}



void buttonPushed(byte b) {
  
    digitalWrite(LED_DEBUG, HIGH); // AS button was pushed
    bButtonPushed = true;
    
    // The power button works regardless of if the unit is in standby mode.
    if (button[b].pin == BTN_POWER) powerButtonPushed();
    
    // are we NOT in standby mode... ?
    if (!settings.standby) {
    
        // Deal with the universal buttons (source-independant buttons).
        universalButtons(button[b].pin);
        
        // While we're in the menu, the source-dependant buttons don't do anything.
        if (bytInputMode == INPUT_MODE_NORMAL) {
        
            switch (settings.source) {
                            
            // Tuner-specific buttons                
            case SOURCE_TUNER:
                tunerButtons(button[b].pin, false);
                break;    
            
            // Line-in specific buttons
            case SOURCE_LINEIN:
                // There are none.
                break;
                
            // USB specific buttons
            case SOURCE_USB:
                usbButtons(button[b].pin);
                break;
            }
        }
    }
}



void buttonHeld(byte b) {
  
    boolean bHoldDidSomething = false;
    
    // are we NOT in standby mode... ?
    if (!settings.standby) {
    
        // Deal with the universal buttons (source-independant buttons).
        universalButtons(button[b].pin);
        
        // While we're in the menu, the source-dependant buttons don't do anything.
        if (bytInputMode == INPUT_MODE_NORMAL) {
        
            switch (settings.source) {
                            
            // Tuner-specific buttons                
            case SOURCE_TUNER:
                tunerButtons(button[b].pin, true);
                break;
            }
        }
    }
    
    // If there's no function set up for holding this button, we'll pretend it was just a push.
    if (!bHoldDidSomething) buttonPushed(b);
}



void powerButtonPushed() {
    
    // Toggle standby mode.
    settings.standby = !settings.standby;
    
    // Turn the external amplifier on/off.
    digitalWrite(PIN_AMP_POWER, settings.standby ? LOW : HIGH);
    
    if (settings.standby) {
        
        // If we're in standby mode, we shut down a few things.
        bMute              = false;
        bSeeking           = false;
        bDisplayingMessage = false;
        bytInputMode       = INPUT_MODE_NORMAL;
        
        // Quiet please!
        audioProcessor.setVolume(map(0, MIN_VOLUME_ENC, MAX_VOLUME_ENC, MIN_VOLUME_PT2314, MAX_VOLUME_PT2314));
        
        // Tuner
        if (settings.source == SOURCE_TUNER) tuner.setVolume(0);
        
        // VMusic3
        if (settings.source == SOURCE_USB) {
            if (vmusic.mode == VMUSIC_MODE_PLAYING) {
                vmusic.pause();
                bResume = true;
            }
        }        
        
    } else {
        
        // turn the source back on.
        initSource();
        audioProcessor.setVolume(map(settings.currentVolume, MIN_VOLUME_ENC, MAX_VOLUME_ENC, MIN_VOLUME_PT2314, MAX_VOLUME_PT2314));
    }
    
    saveSettings();
}



void universalButtons(byte pin) {

    // Universal buttons
    switch (pin) {
   
    case BTN_SOURCE:
        
        // Cycle through the sources (Eg: "Tuner" and "Line-in").
        settings.source++;
        if (settings.source == SOURCES) settings.source = 0;
        initSource();
        bytInputMode = INPUT_MODE_NORMAL;
        saveSettings();
        break;
        
    case BTN_MENU:
        switch (bytInputMode) {
        case INPUT_MODE_NORMAL:
            // Go into the menu    
            bytInputMode = INPUT_MODE_MENU;
            break;
        
        case INPUT_MODE_MENU:
            // Go into the menu item
            bytInputMode = menuItems[intMenuItem].inputMode;
            break;
        
        case INPUT_MODE_BASS:
        case INPUT_MODE_TREBLE:
        case INPUT_MODE_BALANCE:
        case INPUT_MODE_GRAPH_STYLE:
        case INPUT_MODE_BRIGHTNESS:
            bytInputMode = INPUT_MODE_NORMAL; // Exit the menu
            saveSettings();
            break;
            
        case INPUT_MODE_HOURS:
            bytInputMode = INPUT_MODE_MINUTES;
            break;
            
        case INPUT_MODE_MINUTES:
            setDateTime();
            bytInputMode = INPUT_MODE_NORMAL;
            break;
        }
        break; // end case BTN_MENU
        
     case BTN_BACK:
        switch (bytInputMode) {
        case INPUT_MODE_MENU:
            // Exit the menu
            bytInputMode = INPUT_MODE_NORMAL;
            bDisplayingMessage = false;
            break;
        
        case INPUT_MODE_BASS:
        case INPUT_MODE_TREBLE:
        case INPUT_MODE_BALANCE:
        case INPUT_MODE_GRAPH_STYLE:        
        case INPUT_MODE_BRIGHTNESS:   
        case INPUT_MODE_HOURS:   
            // Go back to the menu
            bytInputMode = INPUT_MODE_MENU;
            saveSettings();
            break;
            
        case INPUT_MODE_MINUTES:
            // Go back to the hour
            bytInputMode = INPUT_MODE_HOURS;
            break;
        }
        break; // end case BTN_BACK
        
    case BTN_DISPLAY:
        // Cycle through the display modes.
               
        bDisplayingMessage = false;

        settings.displayMode++;
        if (settings.displayMode == DISPLAY_MODES) settings.displayMode = 0;
        
        // if we're about to show the clock, get the time.
        if (settings.displayMode == DISPLAY_MODE_CLOCK) getDateTime();
        
        bytInputMode = INPUT_MODE_NORMAL;
        saveSettings();
        break; // end case BTN_DISPLAY
        
    case BTN_MUTE:
        bMute = !bMute;
        if (bMute)
        {
            audioProcessor.setVolume(0);
            bDisplayingMessage = false;
        }
        else
        {
            audioProcessor.setVolume(map(settings.currentVolume, MIN_VOLUME_ENC, MAX_VOLUME_ENC, MIN_VOLUME_PT2314, MAX_VOLUME_PT2314));
        }

        bytInputMode = INPUT_MODE_NORMAL;
        break; // end case BTN_MUTE
    }
}



void tunerButtons(byte pin, boolean bHeld) {
    
    switch (pin) {
    
    case BTN_PRESET0:
        presetPushed(0, bHeld);
        break;
    
    case BTN_PRESET1:
        presetPushed(1, bHeld);
        break;
    
    case BTN_PRESET2:
        presetPushed(2, bHeld);
        break;
    
    case BTN_PRESET3:
        presetPushed(3, bHeld);
        break;
    
    case BTN_PRESET4:
        presetPushed(4, bHeld);
        break;
        
    case BTN_SEEKDOWN:
        tuner.startSeekingDown();
        bSeeking = true;
        settings.currentPreset = NO_PRESET;
        break;
        
    case BTN_SEEKUP:
        tuner.startSeekingUp();
        bSeeking = true;
        settings.currentPreset = NO_PRESET;                
        break;
        
    case BTN_PRESET5:
        presetPushed(5, bHeld);
        break;        

    }
}



void usbButtons(byte pin) {
    
    switch (pin) {
    
    case BTN_PLAY:
        if (strlen(vmusic.filename) > 0) {
            if (vmusic.mode == VMUSIC_MODE_PAUSED) vmusic.pause(); // unpause
            else vmusic.play();
            messageTrackInfo();
        }
        break;
    
    case BTN_PAUSE:
        vmusic.pause(); // Pause|Unpause        
        if (vmusic.mode == VMUSIC_MODE_PLAYING) messageTrackInfo();
        else message(MESSAGE_USB_PAUSE, MESSAGE_DURATION);
        break;
    
    case BTN_STOP:
        vmusic.stop();
        message(MESSAGE_USB_STOP, MESSAGE_DURATION);
        break;
    
    case BTN_RANDOM:
        settings.random = !(settings.random);
        vmusic.randomMode = settings.random;
        saveSettings();      
        sprintf(strCurrentMessage, "%s:%s", MESSAGE_USB_RANDOM, settings.random ? "On" : "Off");
        message(strCurrentMessage, MESSAGE_DURATION);
        break;

    case BTN_REPEAT:
        settings.repeatFolder = !(settings.repeatFolder);    
        vmusic.repeatFolder   = settings.repeatFolder;
        saveSettings();
        sprintf(strCurrentMessage, "%s:%s", MESSAGE_USB_REPEAT, settings.repeatFolder ? "On" : "Off");
        message(strCurrentMessage, MESSAGE_DURATION);
        break;
        
    case BTN_SEEKDOWN:
        if (bFolder) vmusic.prevFolder();
        else vmusic.prevTrack();
        messageTrackInfo();
        saveSettings();
        saveLastTrack();
        break;
        
    case BTN_SEEKUP:
        if (bFolder) vmusic.nextFolder();
        else vmusic.nextTrack();
        messageTrackInfo();
        saveSettings();
        saveLastTrack();
        break;
        
    case BTN_FOLDER:
        bFolder = !bFolder;
        messageTrackInfo();
        break;
    }
}



void presetPushed(byte preset, boolean bHeld) {
    
    // preset: 0 ~ 5.
    
    if (!bSeeking) {
        if (bHeld) settings.presetFrequency[preset] = settings.currentFrequency;
        else settings.currentFrequency = settings.presetFrequency[preset];
        settings.currentPreset = preset;    
        bSeeking = false;
        
        tuner.setFrequency(settings.currentFrequency);
        
        saveSettings();
       
        messageCurrentFrequency();
    }
}
