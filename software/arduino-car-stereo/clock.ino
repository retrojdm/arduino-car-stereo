/*------+
| Clock |
+-------+

void setupClock  ()

byte decToBcd    (byte val)
byte bcdToDec    (byte val)
void getDateTime ()
void setDateTime ()
*/



void setupClock() {

    // Initialise the I2C interface for the Realtime Clock
    Wire.begin();
    getDateTime();
}



byte decToBcd(byte val) { return ( (val/10*16) + (val%10) ); }
byte bcdToDec(byte val) { return ( (val/16*10) + (val%16) ); }



void getDateTime() {
    // Gets the date and time from the DS1307 and puts it in the global variable "objDateTime".
    
    // Reset the register pointer
    Wire.beginTransmission(DS1307_I2C_ADDRESS);
    Wire.write((byte)0x00);
    Wire.endTransmission();
 
    Wire.requestFrom(DS1307_I2C_ADDRESS, 7);
 
    // A few of these need masks because certain bits are control bits
    objDateTime.second     = bcdToDec(Wire.read() & 0x7f);
    objDateTime.minute     = bcdToDec(Wire.read());
    objDateTime.hour       = bcdToDec(Wire.read() & 0x3f);  // Need to change this if 12 hour am/pm
    objDateTime.dayOfWeek  = bcdToDec(Wire.read());
    objDateTime.dayOfMonth = bcdToDec(Wire.read());
    objDateTime.month      = bcdToDec(Wire.read());
    objDateTime.year       = bcdToDec(Wire.read());
}



void setDateTime() {
    // Sets the date and time of the DS1307 from the global variable "objDateTime".
    
   Wire.beginTransmission(DS1307_I2C_ADDRESS);   
   Wire.write((byte)0x00);
   
   Wire.write(decToBcd(objDateTime.second) & 0x7f);    // 0 to bit 7 starts the clock
   Wire.write(decToBcd(objDateTime.minute));
   Wire.write(decToBcd(objDateTime.hour));
   Wire.write(decToBcd(objDateTime.dayOfWeek));
   Wire.write(decToBcd(objDateTime.dayOfMonth));
   Wire.write(decToBcd(objDateTime.month));
   Wire.write(decToBcd(objDateTime.year));

   Wire.endTransmission();
}
