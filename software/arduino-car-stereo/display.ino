/*--------+
| Display |
+---------+

// 3x HCMS-29xx's cascaded to form one 12 character alphanumeric LED display.

void setupDisplay               ()
void loopDisplay                ()

void setBrightness              (int intBrightness)
void setDisplay                 (char *strDisplay)
void updateDisplayAndLEDs       ()
void waitForMessage             (long lngTimer)
void message                    (char *strMessage, long lngTimer)
void messageCurrentFrequency    ()
void messageTrackInfo           ()
void setDisplayBar              (char *strLabel, int intValue)
void setDisplayBalanceBar       (char *strLabel, int intValue)
void setDisplayCurrentFrequency ()
void setDisplayRDS              (boolean bForceRefresh)
void setDisplayTrackTime        ()
void displayTime                ()
void displaySettingTime         ()
void displaySetting             ()

void setupSpectrum              ()
void clearPeaks                 ()
void readAndDisplaySpectrum     ()
void resample                   (float *arrIn, int intInCount, float *arrOut, int intOutCount)
*/



void setupDisplay() {
  
    // Setup the MSGEQ7 IC.
    setupSpectrum();
    
    // setup the HCMS-29xx displays.
    myDisplay.begin();
    myDisplay.clear();
    myDisplay.setBrightness(settings.brightness);
    
    // set up the LEDs
    for (byte p = 0; p < PRESETS; p++) pinMode(presetLEDs[p], OUTPUT);
    pinMode(LED_DEBUG, OUTPUT);
    
    // Copy the splash screen to the dot register (only if not in standby mode).
    if (!(settings.standby)) {
        memcpy(myDisplay.dotRegister, splashScreen, 5 * DISPLAY_LENGTH);
        myDisplay.loadDotRegister();
    }
}



void loopDisplay() {
    
    static unsigned long lngMillisDisplayClock    = 0;
    static unsigned long lngMillisDisplaySpectrum = 0;
    static unsigned long lngMillisDisplayRDS      = 0;
        
    if (!settings.standby) {
        
        switch (bytInputMode) {
        case INPUT_MODE_NORMAL:
        
            // Is there a message being displayed ?
            if (bDisplayingMessage) {
              
                // Is it neccessary to scroll?
                // i.e.: Only scroll if the message doesn't fit on one screen.
                if (strlen(strCurrentMessage) > DISPLAY_LENGTH) {
            
                    // Are we due to scroll again?
                    if (millis() >= lngMillisScrollAgain) {
                        
                        // Scroll
                        if (myDisplay.getCursor() > intMaxLeft) {
                            myDisplay.scroll(-1);
                        } else {
                            
                            // If looping forever...
                            if (lngMillisMessageFinished == 0) {
                                myDisplay.setCursor(0); // Loop back?
                                myDisplay.scroll(0);
                            } else {
                                // Finish
                                bDisplayingMessage = false;
                                updateDisplayAndLEDs();
                            }
                        }                            
            
                        // Wait before scrolling one more character
                        if ((myDisplay.getCursor() == 0) || (myDisplay.getCursor() == intMaxLeft)) lngMillisScrollAgain = millis() + MESSAGE_DURATION;
                        else lngMillisScrollAgain = millis() + MESSAGE_SCROLL_STEP;
                    }                  
                
                } else {
            
                    // If the message has a life-span, have we reached it?
                    if ((lngMillisMessageFinished > 0) && (millis() >= lngMillisMessageFinished)) {
                        bDisplayingMessage = false;
                        updateDisplayAndLEDs();
                    }
                }
            
            } else { // Not displaying message
                
                if (!bMute) {
                  
                    // We show the frequency when seeking, ever if we're not in "Info" display mode.
                    if (bSeeking) {
                        if (millis() >= lngSeekDisplayTimer) {
                            lngSeekDisplayTimer = millis() + SEEK_DISPLAY_DELAY;
                            if (settings.source == SOURCE_TUNER) messageCurrentFrequency();
                        }
                        
                    } else { // not seeking
                      
                        // Are we in "clock" display mode?
                        if (settings.displayMode == DISPLAY_MODE_CLOCK) {
                          
                            // has it been over 1 second?
                            if (millis() >= lngMillisDisplayClock) {
                                lngMillisDisplayClock = millis() + 1000;
                                
                                // Check the RTC module
                                int intMinute = objDateTime.minute;
                                getDateTime();
                                if (intMinute != objDateTime.minute) displayTime();
                            }
                        }
                        
                        
                        // Are we in "RDS" display mode?
                        if ((settings.displayMode == DISPLAY_MODE_LONG) && (settings.source == SOURCE_TUNER)) {
                                                
                            if (millis() > lngMillisDisplayRDS) {
                                lngMillisDisplayRDS = millis() + RDS_REFRESH_DELAY;
                                
                                // Try to read the RDS Message again. Only refresh if something's changed.
                                setDisplayRDS(false);
                            }
                        }
                        
  
                        // Are we in "Spectrum Analyzer" display mode?
                        if (settings.displayMode == DISPLAY_MODE_SPECTRUM) {
                                                
                            if (millis() > lngMillisDisplaySpectrum) {
                                lngMillisDisplaySpectrum = millis() + SPECTRUM_DELAY;
                                
                                // Read the 7 band spectrum analyzer (MSGEQ7), and display it.
                                readAndDisplaySpectrum();
                            }
                        }
                    }

                } // End if (!mute)
            } // End else (Not displaying message)
            break; // INPUT_MODE_NORMAL
            
        case INPUT_MODE_HOURS:
        case INPUT_MODE_MINUTES:
            displaySettingTime();
            break;
        }
    }
}



void setBrightness(int intBrightness) {
  
    settings.brightness = constrain(intBrightness, 1, 15);
    myDisplay.setBrightness(settings.brightness);
}



void setDisplay(char *strMessage) {
  
    if (strCurrentMessage != strMessage) sprintf(strCurrentMessage, "%s", strMessage);
    
    // Show the digits.
    myDisplay.setString(strCurrentMessage);
    myDisplay.home();
    
    // Something to display?
    myDisplay.scroll(0);
}



void updateDisplayAndLEDs() {
    
    if (settings.standby) {
        setDisplay("");
    } else {
        
        switch(bytInputMode) {
          
        case INPUT_MODE_NORMAL:
            if (!bDisplayingMessage) {
            
                // Display
                if (bMute) {
                    setDisplay(MESSAGE_MUTE);
                } else {
                    switch (settings.displayMode) {
                    case DISPLAY_MODE_SHORT:
                        
                        switch (settings.source) {
                        case SOURCE_TUNER:
                            setDisplayCurrentFrequency();
                            break;
                            
                        case SOURCE_LINEIN:
                            setDisplay(MESSAGE_LINEIN);
                            break;

                        case SOURCE_USB:
                            setDisplayTrackTime();
                            break;

                        }
                        break;
                        
                    case DISPLAY_MODE_LONG:
                        
                        switch (settings.source) {
                        case SOURCE_TUNER:
                        
                            setDisplayRDS(true); // Force a refresh of the display, even if the RDS message hasn't changed.
                            break;
                            
                        case SOURCE_LINEIN:
                            setDisplay(MESSAGE_LINEIN);
                            break;

                        case SOURCE_USB:
                            messageTrackInfo();
                            break;

                        }
                        break;
                        
                    case DISPLAY_MODE_CLOCK:
                        displayTime();
                        break;
                    }
                }
            }
            break;
            
        case INPUT_MODE_MENU:
        case INPUT_MODE_BASS:
        case INPUT_MODE_TREBLE:
        case INPUT_MODE_BALANCE:
        case INPUT_MODE_GRAPH_STYLE:
        case INPUT_MODE_BRIGHTNESS:
           displaySetting();
           break; 
 
        case INPUT_MODE_HOURS:
        case INPUT_MODE_MINUTES:        
            displaySettingTime();
            break;           
        }
    }
    
    
    // LEDs
    if (settings.standby || (settings.source == SOURCE_LINEIN)) {
      
        for (byte p = 0; p < PRESETS; p++) digitalWrite(presetLEDs[p], LOW); // Presets
        
    } else {

        switch (settings.source) {
        case SOURCE_TUNER:            
            for (byte p = 0; p < PRESETS; p++) digitalWrite(presetLEDs[p], p == settings.currentPreset ? HIGH : LOW); // Presets
            break;
    
        case SOURCE_USB:
            digitalWrite(LED_PLAY,   vmusic.mode == VMUSIC_MODE_PLAYING ? HIGH : LOW);
            digitalWrite(LED_PAUSE,  vmusic.mode == VMUSIC_MODE_PAUSED  ? HIGH : LOW);
            digitalWrite(LED_STOP,   vmusic.mode == VMUSIC_MODE_STOPPED ? HIGH : LOW);
            digitalWrite(LED_RANDOM, settings.random        ? HIGH : LOW);
            digitalWrite(LED_REPEAT, settings.repeatFolder  ? HIGH : LOW);
            digitalWrite(LED_FOLDER, bFolder                ? HIGH : LOW); // Folder LED
            break;
            
        }
    }
}



void waitForMessage(long lngTimer) {
        
    bDisplayingMessage = true;
    
    // Set a delay before we scroll.
    lngMillisScrollAgain = millis() + MESSAGE_SCROLL_PAUSE;
    
    // "life-span" timer (0 = doesn't run out)
    if (lngTimer == 0) lngMillisMessageFinished = 0;
    lngMillisMessageFinished = millis() + MESSAGE_DURATION;
}



void message(char *strMessage, long lngTimer) { 
    
    if (bytInputMode == INPUT_MODE_NORMAL) {
        
        // Copy the message
        if (strCurrentMessage != strMessage) sprintf(strCurrentMessage, "%s", strMessage);
        
        // "life-span" timer (0 = doesn't run out)
        waitForMessage(lngTimer);
                
        // Set up the scroller
        // intMaxLeft should be a negative number if the message is wider than the display.
        // Eg: intMaxLeft = -2 if the first character is 2 positions off to the left of the screen.
        intMaxLeft = DISPLAY_LENGTH - strlen(strCurrentMessage);
        if (intMaxLeft > 0) intMaxLeft = 0;
        myDisplay.setCursor(0);
        myDisplay.setString(strCurrentMessage);
        myDisplay.scroll(0);        
    }
}



void messageCurrentFrequency() { 
     
    sprintf(strCurrentMessage, "FM %3d.%d MHz", settings.currentFrequency / 10, settings.currentFrequency % 10);
    message(strCurrentMessage, MESSAGE_DURATION);
}



void messageTrackInfo() {
  
    if (settings.source == SOURCE_USB) {
      
        if (vmusic.diskPresent) {

            if (bFolder) { // Folder mode
                
                // Is there an Album name to show?
                if ((strlen(vmusic.album) > 0) || (strlen(vmusic.artist) > 0)) {
                    sprintf(strCurrentMessage, "%s (%s)", vmusic.album, vmusic.artist);
                } else {
                    // Show the full path
                    strcpy(strCurrentMessage, MESSAGE_USB_DEVICE_NAME);
                    if (strlen(vmusic.path) > 0) strcat(strCurrentMessage, "\\");
                    strcat(strCurrentMessage, vmusic.path);
                    strcat(strCurrentMessage, "\\");
                }
                // Folder Mode = show indefinately.
                message(strCurrentMessage, 0);
                
            } else { // Track mode
                
                // Is there a Track Title to show
                if (strlen(vmusic.title) > 0) sprintf(strCurrentMessage, "%02u %s", vmusic.trackNumber, vmusic.title);
                else if (strlen(vmusic.filename) > 0) sprintf(strCurrentMessage, "%02u %s", vmusic.trackNumber, vmusic.filename); // otherwise, use the filename.
                else strcpy(strCurrentMessage, MESSAGE_USB_NO_TRACKS);      
                
                // Track mode - show for a few seconds.
                message(strCurrentMessage, MESSAGE_DURATION);
            }
        } else setDisplay(MESSAGE_USB_NO_DISK);
    }
}



void setDisplayBar(char *strLabel, int intValue) {
    
    // Shows the setting as a horizontal bar from 0 to 15.
    
    int x, c, v;
    
    intValue = constrain(intValue, 0, 15);
    
    // Set the current message. This stops the scroll timer if the last message was long.
    strcpy(strCurrentMessage, strLabel);
    
    // Write the label.
    c = 0;
    while ((c < DISPLAY_LENGTH) && strLabel[c] != '\0') {
        myDisplay.writeCharacter(strLabel[c], c);
        c++;
    }
    
    // Clear the rest
    for (; c < DISPLAY_LENGTH; c++) myDisplay.writeCharacter(' ', c);
    
    // Add the bars
    x = 20;
    for (v = 0; v <= intValue; v++) {
        myDisplay.dotRegister[x] = 0x7F;
        x += (v % 2 == 0) ? 4 : 1;
    }
    
    // Show it
    myDisplay.loadDotRegister();
}



void setDisplayBalanceBar(char *strLabel, int intValue) {
    
    // Shows the setting as a horizontal bar, for -7 to +7 with a mark for 0.
    
    intValue = constrain(intValue, -7, 7);
    
    int x, c, v;
    
    // Set the current message. This stops the scroll timer if the last message was long.
    strcpy(strCurrentMessage, strLabel);
    
    // Write the label.
    c = 0;
    while ((c < DISPLAY_LENGTH) && strLabel[c] != '\0') {
        myDisplay.writeCharacter(strLabel[c], c);
        c++;
    }
    
    // Clear the rest
    for (; c < DISPLAY_LENGTH; c++) myDisplay.writeCharacter(' ', c);
    
    // Add the zero mark.
    x = 40;
    myDisplay.dotRegister[x] = 0x7F;
    
    // Add the bars.
    v = 1;
    while (v <= abs(intValue)) {
        if (intValue > 0) x += (v % 2 == 0) ? 1 : 4;
        else x -= (v % 2 == 0) ? 4 : 1;
        myDisplay.dotRegister[x] = 0x3E;
        v++;
    }
    
    // Show it
    myDisplay.loadDotRegister();
}



void setDisplayCurrentFrequency() { 

    // We have to divide by 10, because the frequencies are stored in MHz * 10, to avoid floating point maths.  
    sprintf(strCurrentMessage, "  %3d.%d FM  ", settings.currentFrequency / 10, settings.currentFrequency % 10);
    setDisplay(strCurrentMessage);
}



void setDisplayRDS(boolean bForceRefresh) {

    // Try to read from RDS. If nothing was returned, just displaying the frequency in a longer format.
    tuner.readRDS(strRDSMessage, RDS_TIMEOUT);
    if (strlen(strRDSMessage) == 0) sprintf(strRDSMessage, "FM %3d.%d MHz", settings.currentFrequency / 10, settings.currentFrequency % 10);
    
    // Only update if something's changed. This stops the display flickering.
    // We can force a refresh, when this function is called from outside the loopDisplay() function.
    if (bForceRefresh || (strcmp(strRDSMessage, strCurrentMessage) != 0)) setDisplay(strRDSMessage);
}



void setDisplayTrackTime() {
  
    char strTrackNumber[DISPLAY_LENGTH + 1];
  
    if (vmusic.diskPresent) {
        if (strlen(vmusic.filename) > 0) {
            
            // Create Track Number/Total string
            sprintf(strTrackNumber, "%u/%u", vmusic.trackNumber, vmusic.tracks);
            
            // Track Time - padded with spaces to appear on the right
            sprintf(strCurrentMessage, "%9u:%02u", vmusic.trackSeconds / 60, vmusic.trackSeconds % 60);
            
            // overlay the Track Number/Total string on the left
            for (byte c = 0; c < strlen(strTrackNumber); c++) strCurrentMessage[c] = strTrackNumber[c];

            setDisplay(strCurrentMessage);
        } else setDisplay(MESSAGE_USB_NO_TRACKS);
    
    } else setDisplay(MESSAGE_USB_NO_DISK);
}



void displayTime() {
    
    // Displays the time on the display
    
    byte hour, minute;
    boolean bPM = false;
        
    // What time is it?
    hour   = objDateTime.hour;
    minute = objDateTime.minute;
         
    // 12 hour time
    if (hour >= 12) {
        bPM  = true;
        hour = hour - 12;
    }
    if (hour == 0) hour = 12;
    
    if (bCursor) {

        switch (bytInputMode) {
        case INPUT_MODE_HOURS:
            sprintf(strCurrentMessage, "  __:%02d %s", minute, bPM ? "PM" : "AM");
            break;
        case INPUT_MODE_MINUTES:
            sprintf(strCurrentMessage, "%4d:__ %s", hour, bPM ? "PM" : "AM");
            break;
        default:
            sprintf(strCurrentMessage, "%4d:%02d %s", hour, minute, bPM ? "PM" : "AM");
            break;
        }   
        
    } else {
        sprintf(strCurrentMessage, "%4d:%02d %s", hour, minute, bPM ? "PM" : "AM");
    }

    // Set the display
    setDisplay(strCurrentMessage);
}



void displaySettingTime() {
                
    if (millis() - lngMillisCursorFlash >= CURSOR_FLASH_DURATION) {
        
        displayTime();
        
        bCursor = !bCursor;
        lngMillisCursorFlash = millis();
    }
}



void displaySetting() {
    
    switch (bytInputMode) {
    case INPUT_MODE_MENU:
        sprintf(strCurrentMessage, "%s", menuItems[intMenuItem]);
        setDisplay(strCurrentMessage);
        break;
    case INPUT_MODE_BASS:
        setDisplayBalanceBar("Bas:", settings.bass);
        break;
    case INPUT_MODE_TREBLE:
        setDisplayBalanceBar("Tre:", settings.treble);
        break;
    case INPUT_MODE_BALANCE:
        setDisplayBalanceBar("Bal:", settings.balance);
        break;
    case INPUT_MODE_GRAPH_STYLE:
        sprintf(strCurrentMessage, "Graph: %s", graphStyles[settings.graphStyle]);
        setDisplay(strCurrentMessage);
        break;
    case INPUT_MODE_BRIGHTNESS:
        setDisplayBar("Bri:", settings.brightness);
        break;
    }
}




void setupSpectrum() {
    
    // Setup pins to drive the spectrum analyzer.
    pinMode(PIN_MSGEQ7_RESET, OUTPUT);
    pinMode(PIN_MSGEQ7_STROBE, OUTPUT);
    
    //Init spectrum analyzer
    digitalWrite(PIN_MSGEQ7_STROBE, LOW);
    delay(1);
    digitalWrite(PIN_MSGEQ7_RESET, HIGH);
    delay(1);
    digitalWrite(PIN_MSGEQ7_STROBE, HIGH);
    delay(1);
    digitalWrite(PIN_MSGEQ7_STROBE, LOW);
    delay(1);
    digitalWrite(PIN_MSGEQ7_RESET, LOW);
    delay(5);
    // Reading the analyzer now will read the lowest frequency.
    
    clearPeaks();
    
    // Reset the scale samples.
    for (int s = 0; s < MSGEQ7_SCALE_SAMPLES; s++) scaleSamples[s] = 0;
    scaleIndex = 0;
}



void clearPeaks() {
    
    // Reset the peaks.
    for (int c = 0; c < DISPLAY_LENGTH; c++) {
        peak[c] = 0;
        peakTimer[c] = 0;
    }
}



void readAndDisplaySpectrum() {
    
    // Read 7 band equalizer.    
    // Band 0 = Lowest Frequencies.
    float spectrum[7];
    float spectrumResampled[DISPLAY_LENGTH];
    int c, x, y, s;
    int intMax;
    byte bytLevel, bytPeak;
    unsigned long lngGraphScale;
    
    byte bytPulse[8] = { 0x00, 0x08, 0x0C, 0x1C, 0x1E, 0x3E, 0x3F, 0x7F };
        
    // Read from the MSGEQ7.
    intMax = 0;
    for(byte band = 0; band < 7; band++) {
        spectrum[band] = (analogRead(PIN_MSGEQ7_OUT) + analogRead(PIN_MSGEQ7_OUT) ) >> 1; //Read twice and take the average by dividing by 2
        if (spectrum[band] > intMax) intMax = spectrum[band];
        digitalWrite(PIN_MSGEQ7_STROBE, HIGH);
        digitalWrite(PIN_MSGEQ7_STROBE, LOW);     
    }    
    scaleSamples[scaleIndex] = intMax; // store the max level this frame in the array. This will be used to scale the height of the graph.
    scaleIndex++;
    if (scaleIndex >= MSGEQ7_SCALE_SAMPLES) scaleIndex = 0;
    
    // Get the average of all the samples
    lngGraphScale = 0;
    for (s = 0; s < MSGEQ7_SCALE_SAMPLES; s++) lngGraphScale += scaleSamples[s];
    lngGraphScale /= MSGEQ7_SCALE_SAMPLES;
    
    // We have a minimum scale, so that lower levels don't get scaled up, and make everything look tall.
    if (lngGraphScale < MSGEQ7_MINIMUM_SCALE) lngGraphScale = MSGEQ7_MINIMUM_SCALE;
    

    // Resample the spectrum (from 7 bars to 12).
    resample(spectrum, 7, spectrumResampled, DISPLAY_LENGTH);

    // Display the resampled spectrum across the 12 bars, or only 7 if it's "Bars + Track Time" mode.
    for (c = 0; c < (settings.graphStyle == GRAPH_STYLE_TRACK_TIME ? 7 : DISPLAY_LENGTH); c++) {
        
        // Convert the reading to a bar height.
        y = (settings.graphStyle == GRAPH_STYLE_TRACK_TIME ? spectrum[c] : spectrumResampled[c]);
        if (y < MSGEQ7_NOISE_CUTOFF) y = 0;        
        y = constrain(map(y, 0, lngGraphScale, 0, CHARACTER_HEIGHT), 0, CHARACTER_HEIGHT);
        
        
        // what are we drawing?
        switch (settings.graphStyle) {
        
        case GRAPH_STYLE_LINES:
            bytLevel = (0x80 >> (y > 0 ? y : 1)) & 0x7f;
            bytPeak  = 0x00;
            break;

        case GRAPH_STYLE_BARS:
        case GRAPH_STYLE_TRACK_TIME:
            bytLevel = (0x7f << (CHARACTER_HEIGHT - y)) & 0x7f;
            bytPeak  = 0x00;
            break;
        
        case GRAPH_STYLE_PEAKS:
            bytLevel = (0x7f << (CHARACTER_HEIGHT - y)) & 0x7f;
            
            // find a new peak, or drop it.
            if (settings.graphStyle == GRAPH_STYLE_PEAKS) {
                if (y > peak[c]) {
                    peak[c] = y;
                    peakTimer[c] = PEAK_HOLD_FRAMES;
                } else {
                    if (peakTimer[c] > 0) peakTimer[c]--;
                    else if (peak[c] > 0) peak[c]--;
                }
            }
            bytPeak = (0x80 >> peak[c]) & 0x7f;
            break;
            
        case GRAPH_STYLE_PULSE:
            bytLevel = bytPulse[y];
            bytPeak  = 0x00;
            break;            
        }
        
        // Draw it.
        for (x = 0; x < CHARACTER_WIDTH; x++) myDisplay.dotRegister[c * CHARACTER_WIDTH + x] = bytLevel | bytPeak;
    }
    
    // Show the Track Time / FM Frequency / "Line" on the right?
    if (settings.graphStyle == GRAPH_STYLE_TRACK_TIME) {

        switch (settings.source) {
        case SOURCE_TUNER:
            // FM Frequency
            sprintf(strCurrentMessage, "%10d.%d", settings.currentFrequency / 10, settings.currentFrequency % 10);
            break;
        case SOURCE_USB:
            // Track Time
            if (vmusic.diskPresent) sprintf(strCurrentMessage, "%9u:%02u", vmusic.trackSeconds / 60, vmusic.trackSeconds % 60);
            else sprintf(strCurrentMessage, MESSAGE_USB_NO_DISK);
            break;
        case SOURCE_LINEIN:
            // "Line"
            sprintf(strCurrentMessage, "%12s", MESSAGE_LINEIN_SPECTRUM);
            break;
        }
        
        // Only show the bars if we're NOT in USB mode, OR there's a USB drive present.
        byte bytCharacters = 5;
        if ((settings.source == SOURCE_USB) && !(vmusic.diskPresent)) bytCharacters = DISPLAY_LENGTH;
        for (c = 0; c < bytCharacters; c++)
            myDisplay.writeCharacter(strCurrentMessage[(DISPLAY_LENGTH - 1) - c], (DISPLAY_LENGTH - 1) - c);
    }

    // Finally, shift it out to the display.
    myDisplay.loadDotRegister();
}



void resample(float *arrIn, int intInCount, float *arrOut, int intOutCount) {
  
    float x1, x2;
    float sum;
    
    for (int i = 0; i < intOutCount; i++) {
        
        // How far along the "in" array are the scaled-down borders for the "out" array.
        x1 = (float)intInCount / (float)intOutCount * i;
        x2 = (float)intInCount / (float)intOutCount * (i + 1);

        // Does the "out" element fall completely within an "in" element?
        if ((int)x1 == (int)x2) {
          
            // Use whole "in" element
            arrOut[i] = arrIn[(int)x1];
            
        } else {
            // First part-element
            sum = arrIn[(int)x1] * (1.0f - (x1 - (int)x1));
    
            // inbetween?
            for (int j = (int)x1 + 1; j <= (int)x2 - 1; j++) sum += arrIn[j];
    
            // Last part-element
            sum += arrIn[(int)x2] * (x2 - (int)x2);
    
            arrOut[i] = sum / (x2 - x1);
        }
    }
}
