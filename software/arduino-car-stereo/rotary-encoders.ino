/*----------------+
| Rotary Encoders |
+-----------------+

void setupRotaryEncoders ()
void loopRotaryEncoders  ()

void initEncoder         (struct EncoderType *encEncoder, byte pinA, byte pinB)
int  readEncoder         (struct EncoderType *encEncoder)
void updateEncoder       (struct EncoderType *encEncoder)
void interruptTuning     ()
void interruptVolume     ()
*/



void setupRotaryEncoders() {
    
    // setup the rotary encoders with interrupts
    initEncoder(&encTuning, ENC_TUNING_A, ENC_TUNING_B);
    attachInterrupt(INT_TUNING_A, interruptTuning, CHANGE);
    attachInterrupt(INT_TUNING_B, interruptTuning, CHANGE);    
    
    initEncoder(&encVolume, ENC_VOLUME_A, ENC_VOLUME_B);
    attachInterrupt(INT_VOLUME_A, interruptVolume, CHANGE);
    attachInterrupt(INT_VOLUME_B, interruptVolume, CHANGE);    

    // enable interrupts    
    interrupts();
}


void loopRotaryEncoders() {
    
    signed int intTuningPositions = 0;
    signed int intVolumePositions = 0;
        
    // Tuning Knob
    intTuningPositions = readEncoder(&encTuning);
    if (intTuningPositions && !settings.standby) {
        
        switch (bytInputMode) {   
   
        case INPUT_MODE_NORMAL:
            switch (settings.source) {
            case SOURCE_TUNER:
            
                settings.currentFrequency = constrain(settings.currentFrequency + (intTuningPositions * FREQUENCY_STEP), MIN_FREQUENCY, MAX_FREQUENCY);
                tuner.setFrequency(settings.currentFrequency);
                saveSettings();
                
                messageCurrentFrequency();
                
                // Are we on a preset?
                if (settings.currentPreset) {
                    // turn off the LEDs
                    settings.currentPreset = NO_PRESET;
                    for (byte p = 0; p < PRESETS; p++) digitalWrite(presetLEDs[p], LOW);
                }
                break;
                
            case SOURCE_USB:
                if (intTuningPositions < 0) vmusic.rewind();
                else if (intTuningPositions > 0) vmusic.fastForward();
                
                // Show the track time, and treat it as a message (stays on the display for a while)
                setDisplayTrackTime();
                bDisplayingMessage = true;
                lngMillisMessageFinished = millis() + MESSAGE_DURATION;                
                
                break;
                
            }
            break;  
    
        case INPUT_MODE_MENU:
            intMenuItem = constrain(intMenuItem + intTuningPositions, 0, MENU_ITEMS - 1);
            displaySetting();
            break;            
            
        case INPUT_MODE_BASS:
            settings.bass = constrain(settings.bass + intTuningPositions, -7, 7);
            audioProcessor.setBass(settings.bass);
            displaySetting();
            break;
            
        case INPUT_MODE_TREBLE:
            settings.treble = constrain(settings.treble + intTuningPositions, -7, 7);
            audioProcessor.setTreble(settings.treble);
            displaySetting();
            break;
            
        case INPUT_MODE_BALANCE:
            settings.balance = constrain(settings.balance + intTuningPositions, -31, 31);
            audioProcessor.setBalance(settings.balance);
            displaySetting();
            break;
            
       case INPUT_MODE_GRAPH_STYLE:
            settings.graphStyle = constrain(settings.graphStyle + intTuningPositions, 0, GRAPH_STYLES - 1);
            clearPeaks();
            displaySetting();
            break;
            
       case INPUT_MODE_BRIGHTNESS:
            setBrightness(settings.brightness + intTuningPositions);
            displaySetting();
            break;
     
        case INPUT_MODE_HOURS:
            bCursor = false;
            objDateTime.hour = (24 + objDateTime.hour + intTuningPositions) % 24;
            displayTime();
            break;
            
        case INPUT_MODE_MINUTES:
            bCursor = false;
            objDateTime.minute = (60 + objDateTime.minute + intTuningPositions) % 60;
            displayTime();
            break;
        }
    }
    
    
    // Volume Knob
    intVolumePositions = readEncoder(&encVolume);
    if (intVolumePositions && !settings.standby) {
        
        // stop the pulses from making the volume negative.
        settings.currentVolume = constrain(settings.currentVolume + intVolumePositions, MIN_VOLUME_ENC, MAX_VOLUME_ENC);
        audioProcessor.setVolume(map(settings.currentVolume, MIN_VOLUME_ENC, MAX_VOLUME_ENC, MIN_VOLUME_PT2314, MAX_VOLUME_PT2314));
        saveSettings();
        
        // If not in the menu, display the volume bar.
        if (!((bytInputMode >= INPUT_MODE_MENU) && (bytInputMode <= INPUT_MODE_MENU + MENU_ITEMS))) {
            setDisplayBar("Vol:", settings.currentVolume);
            waitForMessage(MESSAGE_DURATION);
        }
    }       
}


void initEncoder(struct EncoderType *encEncoder, byte pinA, byte pinB) {
    
    pinMode(pinA, INPUT_PULLUP);
    pinMode(pinB, INPUT_PULLUP);
        
    encEncoder->pinA       = pinA;
    encEncoder->pinB       = pinB;
    encEncoder->was        = (digitalRead(pinB) * 2) + digitalRead(pinA);
    encEncoder->is         = encEncoder->was;
    encEncoder->positions  = 0;
    encEncoder->lastUpdate = millis();
}



int readEncoder(struct EncoderType *encEncoder) {
  
    // Settle on a detent if it's been a while.
    if (millis() - encEncoder->lastUpdate >= DETENT_SETTLE_DELAY) encEncoder->positions = 0;
    
    // We'll return the number of full detents rotated past
    int intPositions = encEncoder->positions / POSITIONS_PER_DETENT;
    
    // Keep track of the remainder
    encEncoder->positions = encEncoder->positions % POSITIONS_PER_DETENT;
    
    return intPositions;
}



void updateEncoder(struct EncoderType *encEncoder) {
    
    boolean bForward, bReverse;

    encEncoder->was        = encEncoder->is;
    encEncoder->is         = (digitalRead(encEncoder->pinB) * 2) + digitalRead(encEncoder->pinA);
    encEncoder->lastUpdate = millis();
    
    if (encEncoder->is != encEncoder->was) {
        // "Forward" is shown by the position going from (0 to 1) or (1 to 3)
        // or (3 to 2) or (2 to 0).  Anything else indicates that the user is
        // turning the device the other way.  Remember: this is Gray code, not
        // binary.
        
        bForward = ((encEncoder->was == 0) && (encEncoder->is == 1)) ||
                   ((encEncoder->was == 1) && (encEncoder->is == 3)) ||
                   ((encEncoder->was == 3) && (encEncoder->is == 2)) ||
                   ((encEncoder->was == 2) && (encEncoder->is == 0));
                
        encEncoder->positions += (bForward ? 1 : -1);
    }
}


void interruptTuning() {
    updateEncoder(&encTuning);
}



void interruptVolume() {
    updateEncoder(&encVolume);
}
