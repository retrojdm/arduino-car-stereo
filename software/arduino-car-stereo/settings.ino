/*---------+
| Settings |
+----------+

void saveSettings  ()
void loadSettings  ()
void saveLastTrack ()
void loadLastTrack ()
*/



void saveSettings() {
    byte EEPROMaddress = SETTINGS_ADDRESS;
    const byte* pointer = (const byte*)(const void*)&settings;
    for (unsigned int i = 0; i < sizeof(settings); i++) EEPROM.write(EEPROMaddress++, *pointer++);
}



void loadSettings() {
  
    // Try to load the settings.
    byte EEPROMaddress = SETTINGS_ADDRESS;    
    byte* pointer = (byte*)(void*)&settings;
    for (unsigned int i = 0; i < sizeof(settings); i++) *pointer++ = EEPROM.read(EEPROMaddress++);
    
    // Not ID'd as Arduino Electronic Tuner (AET) Settings?
    if (strcmp(settings.id, SETTINGS_ID_STRING) != 0) {
      
        // Use the default settings...
        strcpy(settings.id, SETTINGS_ID_STRING);
        settings.standby            = false;
        settings.source             = SOURCE_TUNER;
        settings.displayMode        = DISPLAY_MODE_LONG;
        settings.graphStyle         = GRAPH_STYLE_BARS;
        settings.currentPreset      = 2;
        settings.presetFrequency[0] = 909;
        settings.presetFrequency[1] = 925;
        settings.presetFrequency[2] = 977;
        settings.presetFrequency[3] = 1029;
        settings.presetFrequency[4] = 1053;
        settings.presetFrequency[5] = 1077;
        settings.currentFrequency   = 977;
        settings.currentVolume      = 7;
        settings.bass               = 0;
        settings.treble             = 0;
        settings.balance            = 0;
        settings.brightness         = 7;
        settings.random             = false;
        settings.repeatFolder       = false;
        
        // Clear the "Last Track" path.
        EEPROMaddress = SETTINGS_ADDRESS + sizeof(settings);
        EEPROM.write(EEPROMaddress, 0);
    }
}



void saveLastTrack() {
  
    strlcpy(lastTrack.filename, vmusic.filename, sizeof(lastTrack.filename));
    strlcpy(lastTrack.path, vmusic.path, sizeof(lastTrack.path));
    lastTrack.trackNumber = vmusic.trackNumber;

    byte EEPROMaddress = SETTINGS_ADDRESS + sizeof(settings);
    const byte* pointer = (const byte*)(const void*)&lastTrack;
    for (unsigned int i = 0; i < sizeof(lastTrack); i++) EEPROM.write(EEPROMaddress++, *pointer++);
}



void loadLastTrack() {
    byte EEPROMaddress = SETTINGS_ADDRESS + sizeof(settings);    
    byte* pointer = (byte*)(void*)&lastTrack;
    for (unsigned int i = 0; i < sizeof(lastTrack); i++) *pointer++ = EEPROM.read(EEPROMaddress++);
}
