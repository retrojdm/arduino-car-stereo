/*--------+
| Sources |
+---------+

void setupSource        ()
void loopSource         ()

void initSource         ()
void tryToLoadLastTrack ()
void initAudioProcessor ()
void selectTuner        ()
void deselectTuner      ()
void selectVMusic       ()
void deselectVMusic     ()
void selectLineIn       ()
void deselectLineIn     ()
*/



void setupSource() {
  
    char strMessage[BUFFER_LENGTH + 1];
    
    // 5V Relay for sending a 12V "Remote Power On" signal to am external Amplifier.
    // This will be set to HIGH when the electronic tuner is switched on, and LOW when switched off.
    pinMode(PIN_AMP_POWER, OUTPUT);
    digitalWrite(PIN_AMP_POWER, settings.standby ? LOW : HIGH);
  
    // Start the tuner.
    tuner.powerOn();
    tuner.setFrequency(settings.currentFrequency);
    
    // Power up the VMusic3
    pinMode(PIN_VMUSIC_5V, OUTPUT);
    digitalWrite(PIN_VMUSIC_5V, HIGH);
   
    vmusic.keepPlaying  = true;
    vmusic.repeatFolder = settings.repeatFolder;
    vmusic.randomMode   = settings.random;
    
    // Set up the PT2314 (Volume, Bass, Treble, Balance, 4 audio input switch)
    audioProcessor.setVolume(settings.standby ? 0 : map(settings.currentVolume, MIN_VOLUME_ENC, MAX_VOLUME_ENC, MIN_VOLUME_PT2314, MAX_VOLUME_PT2314));
    audioProcessor.setBass(settings.bass);
    audioProcessor.setTreble(settings.treble);
                
    // Start whatever source is loaded from the settings.
    initSource();
}



void loopSource() {
    
    if (bSeeking) {
    
        // Display the frequency as we seek
        bValid = false;
        bDisplayingMessage = false;
        settings.currentFrequency = tuner.getFrequency();

        bValid = tuner.seekTuneComplete();
        if (bValid) {
            bSeeking = false;
            int intStatus = tuner.seekStatus();
            settings.currentFrequency = tuner.getFrequency();
            
            // If it stopped on one of the presets, light it's LED.
            for (int p = 0; p < PRESETS; p++)
                if (settings.currentFrequency == settings.presetFrequency[p])  {
                    settings.currentPreset = p;
                    break;
                }
            
            // Show the changes.
            if (settings.source == SOURCE_TUNER) {
                updateDisplayAndLEDs();
                messageCurrentFrequency();
            }
        }
    }    
    
    switch (vmusic.loop()) {

    case VMUSIC_EVENT_NONE:
        break;

    case VMUSIC_EVENT_INITIALISED:
        if (settings.source == SOURCE_USB) vmusic.setVolume(VMUSIC_VOLUME_MAX);
        if (vmusic.diskPresent) tryToLoadLastTrack();
        break;
        
    case VMUSIC_EVENT_USB_DETECTED:
        if (vmusic.initialised) tryToLoadLastTrack();
        break;

    case VMUSIC_EVENT_USB_REMOVED:
        // Removed? Forget the track we're on.
        lastTrack.path[0]      = '\0';
        lastTrack.filename[0]  = '\0';
        lastTrack.trackNumber  = 0;
        saveLastTrack();
        break;
    
    case VMUSIC_EVENT_TRACK_CHANGED:
        // Remember the track we're on.
        saveLastTrack();
        
        // display it.
        if ((settings.source == SOURCE_USB) && (!(settings.standby))) messageTrackInfo();
        break;
    
    case VMUSIC_EVENT_TRACK_STOPPED:
    case VMUSIC_EVENT_TRACK_PROGRESSED:
        if ((settings.source == SOURCE_USB) && (settings.displayMode == DISPLAY_MODE_SHORT) && (!(settings.standby))) updateDisplayAndLEDs();
        break;

    }
}


void tryToLoadLastTrack() {
    if ((settings.source == SOURCE_USB) && (!(settings.standby)))
        setDisplay(MESSAGE_SEARCHING);
        
    // try to load the track from the previous session.
    if (strlen(lastTrack.filename) > 0) {
        vmusic.loadTrack(lastTrack.path, lastTrack.filename, lastTrack.trackNumber, VMUSIC_MODE_STOPPED);
        if ((settings.source == SOURCE_USB) && (!(settings.standby))) messageTrackInfo();
    }
}


void initSource() {
    
    initAudioProcessor();
    
    // Tuner
    if (settings.source == SOURCE_TUNER) selectTuner();
    else deselectTuner();

    // VMusic3
    if (settings.source == SOURCE_USB) selectVMusic();
    else deselectVMusic();
   
    // Line-in
    if (settings.source == SOURCE_LINEIN) selectLineIn();
    else deselectLineIn();
}


void initAudioProcessor() {
    audioProcessor.setSource(settings.source, false, 3);
    audioProcessor.setBalance(settings.balance);
    if (!(settings.standby)) updateDisplayAndLEDs();
}


void selectTuner() {
    tuner.setVolume(15);
    if (!(settings.standby)) messageCurrentFrequency();
}


void deselectTuner() {
  tuner.setVolume(0);
}


void selectVMusic() {
  if (!vmusic.initialised) return;
  
  if (bResume) {
    vmusic.pause();
    bResume = false;
  }
  vmusic.setVolume(VMUSIC_VOLUME_MAX);
  
  if (!(settings.standby)) messageTrackInfo();
}


void deselectVMusic() {
  if (!vmusic.initialised) return;

  vmusic.setVolume(0);
  
  if (vmusic.mode == VMUSIC_MODE_PLAYING) {
    vmusic.pause();
    bResume = true;
  }
}


void selectLineIn() {
  if (!(settings.standby)) message(MESSAGE_LINEIN, MESSAGE_DURATION);
}


void deselectLineIn() {
  
}
